#!/bin/bash
#
#   Copyright (C) 2015-2025. Vinari Software.
#   All rights reserved.
#
#
#   Redistribution and use in source and binary forms, source code with or without
#   modification, are permitted provided that the following conditions are met:
#
#   1. Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
#   3. All advertising materials mentioning features or use of this software
#   must display the following acknowledgement:
#   This product includes software developed by Vinari Software.
#
#   4. Neither the name of the Vinari Software nor the
#   names of its contributors may be used to endorse or promote products
#   derived from this software without specific prior written permission.
#
#
#   THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
#   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
#   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#   VINARI SOFTWARE & 2015-2025

set -e

function rootCheck(){
	if [ "$USER" != "root" ]; then
		echo "You need to have root permission to install PowerDown - Vinari Software."
		exit 1
	fi
}

function removeFiles(){
	sudo rm -rf /usr/share/vinarisoftware/PowerDown/
	sudo rm -rf /usr/share/doc/powerdown-vinarisoftware/

	sudo rm -rf /usr/share/polkit-1/actions/org.vinarisoftware.powerdown.policy
	sudo rm -rf /usr/share/applications/org.vinarisoftware.powerdown.desktop
	sudo rm -rf /usr/bin/org.vinarisoftware.powerdown
	sudo rm -rf /usr/share/icons/default/PowerDown-Icon.png
	sudo rm -rf /usr/share/glib-2.0/schemas/org.vinarisoftware.powerdown.gschema.xml

	sudo rm -rf /usr/share/locale/de/LC_MESSAGES/org.vinarisoftware.powerdown.mo
	sudo rm -rf /usr/share/locale/es/LC_MESSAGES/org.vinarisoftware.powerdown.mo
	sudo rm -rf /usr/share/locale/fr/LC_MESSAGES/org.vinarisoftware.powerdown.mo
	sudo rm -rf /usr/share/locale/it/LC_MESSAGES/org.vinarisoftware.powerdown.mo
	sudo rm -rf /usr/share/locale/pt/LC_MESSAGES/org.vinarisoftware.powerdown.mo

	sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
}

function makeExecutable(){
	chmod +x data/make-schema.sh
	chmod +x po/make-mo.sh
	chmod +x build/org.vinarisoftware.powerdown
}

function createDirs(){
	sudo mkdir -vp /usr/share/vinarisoftware/PowerDown/
	sudo mkdir -vp /usr/share/doc/powerdown-vinarisoftware/
	sudo mkdir -vp /usr/share/icons/default/
}

function copyFiles(){
	sudo cp -vr Assets/ /usr/share/vinarisoftware/PowerDown/
	sudo cp -v LICENSE /usr/share/doc/powerdown-vinarisoftware/
	sudo cp -v org.vinarisoftware.powerdown.policy /usr/share/polkit-1/actions/
	sudo cp -v data/org.vinarisoftware.powerdown.desktop /usr/share/applications/
	sudo cp -v build/org.vinarisoftware.powerdown /usr/bin/
	sudo cp -v Assets/PowerDown-Icon.png /usr/share/icons/default/
	sudo strip /usr/bin/org.vinarisoftware.powerdown
}

function installTranslations(){
	cd po/
	./make-mo.sh --install
	cd ..
}

function installGschema(){
	cd data/
	./make-schema.sh
	cd ..
}

rootCheck

case $1 in
	--remove | -r)
		removeFiles
		;;

	*)
		echo "Before proceeding make sure that you've compiled the application successfully."
		read -p " Press RETURN to continue."

		makeExecutable
		createDirs
		copyFiles
		installTranslations
		installGschema
		;;
esac

exit 0
