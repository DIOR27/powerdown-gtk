#!/bin/bash
#
#   Copyright (C) 2015-2025. Vinari Software.
#   All rights reserved.
#   
#   
#   Redistribution and use in source and binary forms, source code with or without
#   modification, are permitted provided that the following conditions are met:
#
#   1. Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#   
#   2. Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#   
#   3. All advertising materials mentioning features or use of this software
#   must display the following acknowledgement:
#   This product includes software developed by Vinari Software.
#   
#   4. Neither the name of the Vinari Software nor the
#   names of its contributors may be used to endorse or promote products
#   derived from this software without specific prior written permission.
#   
#   
#   THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
#   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#   DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
#   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#   
#   VINARI SOFTWARE & 2015-2025

set -e

applicationName="org.vinarisoftware.powerdown"
green=$(tput setaf 2)
reset=$(tput sgr0)

function rootCheck(){
	if [ "$USER" != "root" ]; then
		echo "You need root privileges to run this script..."
		exit 1
	fi
}

function copyToRoot(){
	cp -v locale/de/LC_MESSAGES/$applicationName.mo /usr/share/locale/de/LC_MESSAGES/
	cp -v locale/es/LC_MESSAGES/$applicationName.mo /usr/share/locale/es/LC_MESSAGES/
	cp -v locale/fr/LC_MESSAGES/$applicationName.mo /usr/share/locale/fr/LC_MESSAGES/
	cp -v locale/it/LC_MESSAGES/$applicationName.mo /usr/share/locale/it/LC_MESSAGES/
	cp -v locale/pt/LC_MESSAGES/$applicationName.mo /usr/share/locale/pt/LC_MESSAGES/
}

mkdir -v locale/

mkdir -pv locale/de/LC_MESSAGES/
mkdir -pv locale/es/LC_MESSAGES/
mkdir -pv locale/fr/LC_MESSAGES/
mkdir -pv locale/it/LC_MESSAGES/
mkdir -pv locale/pt/LC_MESSAGES/

msgfmt -o locale/de/LC_MESSAGES/$applicationName.mo de.po
msgfmt -o locale/es/LC_MESSAGES/$applicationName.mo es.po
msgfmt -o locale/fr/LC_MESSAGES/$applicationName.mo fr.po
msgfmt -o locale/it/LC_MESSAGES/$applicationName.mo it.po
msgfmt -o locale/pt/LC_MESSAGES/$applicationName.mo pt.po

if [ "$1" == "--install" ]; then
	rootCheck
	copyToRoot
fi

echo
echo "${green}All done${reset}"

exit 0
