/*
	Copyright (C) 2015-2020. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. All advertising materials mentioning features or use of this software
	must display the following acknowledgement:
	This product includes software developed by Vinari Software.

	4. Neither the name of the Vinari Software nor the
	names of its contributors may be used to endorse or promote products
	derived from this software without specific prior written permission.


	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


	VINARI SOFTWARE 2015-2020
*/

using Gtk;
using App.Widgets;


namespace GUI{
	public class MainWindow : Gtk.ApplicationWindow {

		//Objects that will be used in the window
		private AppHeaderBar headerBar;
		private Gtk.Button shutDownButton;
		private Gtk.Button rebootButton;
		private Gtk.Button cancelButton;
		private Gtk.PopoverMenu popMenu;
		private Gtk.ModelButton preferencesButton;
		private Gtk.ModelButton aboutButton;
		private Gtk.ModelButton quitButton;
		private App.Widgets.MessageDialog MsgDialog;

		//Variables that will be used by the application
		public static uint delayTime;
		private bool previousAction;
		private GLib.Settings mySettings=new GLib.Settings("org.vinarisoftware.powerdown");

		//Constructor of the class
		public MainWindow(Application aplicacionV){
			Object(application: aplicacionV);
		}

		//Constructor that builds the window
		construct{
			//Settings retrieval
			this.move(mySettings.get_int("pos-x"), mySettings.get_int("pos-y"));
			delayTime=mySettings.get_uint("delay-time");

			//General window configurations
			this.set_resizable(false);
			this.window_position=Gtk.WindowPosition.CENTER;
			this.set_default_size(575, 250);
			this.set_border_width(10);

			//Icon retrieval
			try{
				Gdk.Pixbuf mainIcon=new Gdk.Pixbuf.from_file(Application.AssetLocation+"PowerDown-Icon.png");
				this.set_icon(mainIcon);
			}catch(Error e){
				error ("Unable to load file: %s", e.message);
			}

			//Delete event setup and configuration
			delete_event.connect(e =>{
				return before_destroy();
			});

			//Box setup
			Gtk.Box verticalBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 7);
			Gtk.Box horizontalBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box menuBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);

			//App header setup
			headerBar=new AppHeaderBar();

			//Button boxes setup
			Gtk.Box shutDownBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
			Gtk.Box rebootBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
			Gtk.Box cancelBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);

			//Buttons setup
			shutDownButton=new Gtk.Button();
			rebootButton=new Gtk.Button();
			cancelButton=new Gtk.Button();

			shutDownButton.set_size_request(140,140);
			rebootButton.set_size_request(140, 140);

			//Icons that will be used setup
			Gtk.Image cancelIcon=new Gtk.Image.from_file(Application.AssetLocation+"Cancel.png");
			Gtk.Image shutDownIcon=new Gtk.Image.from_file(Application.AssetLocation+"ShutDown.png");
			Gtk.Image rebootIcon=new Gtk.Image.from_file(Application.AssetLocation+"Reboot.png");
			Gtk.Image vinariBanner=new Gtk.Image.from_file(Application.AssetLocation+"Banner.png");
			//Labels that will be used setup
			Gtk.Label shutDownLabel=new Label(_("Shutdown"));
			Gtk.Label rebootLabel=new Label(_("Reboot"));
			Gtk.Label cancelLabel=new Label(_("Cancel"));

			//Button boxes configuration
			shutDownBox.pack_start(shutDownIcon, true, false, 1);
			shutDownBox.pack_start(shutDownLabel, true, false, 1);

			rebootBox.pack_start(rebootIcon, true, false, 1);
			rebootBox.pack_start(rebootLabel, true, false, 1);

			cancelBox.pack_start(cancelIcon, true, false, 1);
			cancelBox.pack_start(cancelLabel, true, false, 1);

			//Boxes are added to the buttons
			shutDownButton.add(shutDownBox);
			rebootButton.add(rebootBox);
			cancelButton.add(cancelBox);

			//Deactivation of the cancel button
			cancelButton.set_sensitive(false);

			//Configuration of the main boxes
			horizontalBox.pack_start(shutDownButton, false, true, 0);
			horizontalBox.pack_start(rebootButton, false, true, 0);
			horizontalBox.pack_start(cancelButton, true, true, 0);

			verticalBox.pack_start(horizontalBox, true, true, 0);
			verticalBox.pack_start(vinariBanner, false, true, 0);
			vinariBanner.set_halign(START);

			//Popover menu setup
			popMenu=new Gtk.PopoverMenu();

			//Popover menu items setup and configuration
			preferencesButton=new Gtk.ModelButton();
			preferencesButton.set_label(_("Settings"));
			aboutButton=new Gtk.ModelButton();
			aboutButton.set_label(_("About"));
			quitButton=new Gtk.ModelButton();
			quitButton.set_label(_("Quit"));

			//Popover menu box configuration
			menuBox.pack_start(preferencesButton, true, true, 2);
			menuBox.pack_start(aboutButton, true, true, 2);
			menuBox.pack_start(quitButton, true, true, 2);

			popMenu.add(menuBox);
			popMenu.set_position(Gtk.PositionType.BOTTOM);

			//Connecting all the buttons signals
			headerBar.menuButton.clicked.connect(menuButtonAction);
			preferencesButton.clicked.connect(preferencesButtonAction);
			quitButton.clicked.connect(quitButtonAction);
			aboutButton.clicked.connect(aboutButtonAction);
			cancelButton.clicked.connect(cancelButtonAction);
			rebootButton.clicked.connect(rebootButtonAction);
			shutDownButton.clicked.connect(shutDownButtonAction);

			//Adding the main Box and HeaderBar to the window
			this.add(verticalBox);
			this.set_titlebar(headerBar);
			this.show_all();
		}

		private void menuButtonAction(){
			popMenu.set_relative_to(headerBar.menuButton);
			popMenu.set_modal(true);
			popMenu.show_all();
			popMenu.popup();
		}

		private void preferencesButtonAction(){
			SettingsWindow settingsForm=new SettingsWindow();
			settingsForm.set_transient_for(this);
			settingsForm.set_currentDelayTime(delayTime);
			settingsForm.show_all();
		}

		private void quitButtonAction(){
			before_destroy();
			this.destroy();
		}

		private void aboutButtonAction(){
			AboutWindow aboutForm=new AboutWindow();
			aboutForm.set_transient_for(this);
			aboutForm.show_all();
		}

		private void cancelButtonAction(){
			int exitCode=Posix.system("shutdown -c");
			if(exitCode!=0){
				MsgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("An error has occurred while trying to cancel the scheduled action..."), "PowerDown - Vinari Software");
				return;
			}

			previousAction=false;
			cancelButton.set_sensitive(false);
			MsgDialog=new App.Widgets.MessageDialog(this, INFO, OK, _("The established action has been canceled successfully."), "PowerDown - Vinari Software");
		}

		private void rebootButtonAction(){
			if(previousAction==true){
				MsgDialog=new App.Widgets.MessageDialog(this, WARNING, OK, _("There is already an scheduled action.\nPlease cancel it, and try again."), "PowerDown - Vinari Software");
				return;
			}else{
				int exitCode=Posix.system("shutdown -r "+ delayTime.to_string());
				if(exitCode!=0){
					MsgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("An error has occurred while trying to reboot the computer."), "PowerDown - Vinari Software");
					return;
				}

				previousAction=true;
				cancelButton.set_sensitive(true);
				MsgDialog=new App.Widgets.MessageDialog(this, INFO, OK, _("The computer will reboot in ")+delayTime.to_string()+_(" minute(s)."), "PowerDown - Vinari Software");
			}
		}

		private void shutDownButtonAction(){
			if(previousAction==true){
				MsgDialog=new App.Widgets.MessageDialog(this, WARNING, OK, _("There is already an scheduled action.\nPlease cancel it, and try again."), "PowerDown - Vinari Softawre");
				return;
			}else{
				int exitCode=Posix.system("shutdown -h "+ delayTime.to_string());
				if(exitCode!=0){
					MsgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("An error has occurred while trying to shutdown the computer."), "PowerDown - Vinari Software");
					return;
				}

				previousAction=true;
				cancelButton.set_sensitive(true);
				MsgDialog=new App.Widgets.MessageDialog(this, INFO, OK, _("The computer will shutdown in ")+delayTime.to_string()+_(" minute(s)."), "PowerDown - Vinari Software");
			}
		}

		public bool before_destroy(){
			int posX, posY;
			this.get_position(out posX, out posY);

			mySettings.set_int("pos-x", posX);
			mySettings.set_int("pos-y", posY);
			return false;
		}

		/*
		//Function dedicated to generate notifications
		private void generateNotification(string notifID, string notifMessage){
			var app=get_application();
			var notification = new Notification ("PowerDown - Vinari Software");
			notification.set_priority(HIGH);
			notification.set_body(notifMessage);
			app.send_notification (notifID, notification);
		}
		*/
	}
}
