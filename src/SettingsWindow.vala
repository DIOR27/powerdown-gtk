/*
	Copyright (C) 2015-2020. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. All advertising materials mentioning features or use of this software
	must display the following acknowledgement:
	This product includes software developed by Vinari Software.

	4. Neither the name of the Vinari Software nor the
	names of its contributors may be used to endorse or promote products
	derived from this software without specific prior written permission.


	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


	VINARI SOFTWARE 2015-2020
*/

using Gtk;

namespace GUI{
	public class SettingsWindow : Gtk.ApplicationWindow {

		//Private variables that will be used in this window
		private Gtk.Button okButton;
		private Gtk.Entry delayEntry;
		private Gtk.Label warningLabel;
		private Gtk.CheckButton rememberDelayCheck;
		private GLib.Settings mySettings=new GLib.Settings("org.vinarisoftware.powerdown");

		//Variables needed for this window
		private uint currentDelayTime;
		private uint previousDelayTime;
		private bool isNumeric;
		private bool rememberDelay;

		construct{
			rememberDelay=mySettings.get_boolean("remember-time");

			this.set_title(_("PowerDown - Vinari Software | Settings"));
			this.set_modal(true);
			this.set_resizable(false);
			this.window_position=Gtk.WindowPosition.CENTER_ON_PARENT;
			this.set_border_width(10);

			try{
				Gdk.Pixbuf mainIcon=new Gdk.Pixbuf.from_file(Application.AssetLocation+"PowerDown-Icon.png");
				this.set_icon(mainIcon);
			}catch(Error e){
				error ("Unable to load file: %s", e.message);
			}

			warningLabel=new Gtk.Label(_("Type the amount of time you\nwant to delay the shutdown or\nreboot of the computer."));
			okButton=new Gtk.Button();
			okButton.set_label(_("Apply changes"));
			delayEntry=new Gtk.Entry();
			rememberDelayCheck=new Gtk.CheckButton.with_label(_("Remember last time set."));

			if(rememberDelay==true){
				rememberDelayCheck.set_active(true);
			}else{
				rememberDelayCheck.set_active(false);
			}

			Gtk.Box boxSettings1=new Gtk.Box(Gtk.Orientation.VERTICAL, 15);
			Gtk.Box boxSettings2=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 7);
			Gtk.Box boxSettings3=new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

			boxSettings3.pack_start(delayEntry, false, true, 0);

			boxSettings2.pack_start(warningLabel, false, true, 0);
			boxSettings2.pack_start(boxSettings3, false, true, 0);
			boxSettings2.pack_start(rememberDelayCheck, false, true, 0);

			boxSettings1.pack_start(boxSettings2, false, true, 0);
			boxSettings1.pack_start(okButton, false, true, 0);

			okButton.clicked.connect(okButtonAction);
			this.key_press_event.connect(key_shortcuts);

			delete_event.connect(e =>{
				return before_destroy();
			});

			this.add(boxSettings1);
		}

		private bool key_shortcuts(Gdk.EventKey key){
			switch(key.keyval){
				case Gdk.Key.Escape:{
					this.destroy();
				}
				break;
			}
			return false;
		}

		public void set_currentDelayTime(uint Value){
			this.currentDelayTime=Value;
			delayEntry.set_text(currentDelayTime.to_string());
		}

		private void okButtonAction(){
			try{
				Regex regex = new Regex ("^[0-9 ]+$");
				isNumeric=regex.match(delayEntry.get_text());
			}catch(Error e){
				error("Error: %s", e.message);
			}

			if(isNumeric==true){
				previousDelayTime=currentDelayTime;
				currentDelayTime=int.parse(delayEntry.get_text());
				if(currentDelayTime<400000){
					MainWindow.delayTime=this.currentDelayTime;
					before_destroy();
					this.destroy();
				}
			}
		}

		public bool before_destroy(){
			rememberDelay=rememberDelayCheck.get_active();
			mySettings.set_boolean("remember-time", rememberDelay);

			if(rememberDelay==false){
				mySettings.set_uint("delay-time", previousDelayTime);
			}else{
				mySettings.set_uint("delay-time", currentDelayTime);
			}
			return false;
		}
	}
}
